provider "aws" {
  alias = "sl"
  region = "ap-northeast-2"
  access_key = var.AWS_ACCESS_KEY_ID_DEFAULT
  secret_key = var.AWS_SECRET_ACCESS_KEY_DEFAULT
}

terraform {
 backend "s3" {
  bucket = "tfstate-kbangtest"
  key = "terraform.tfstate"
  region = "ap-northeast-2"
  encrypt = true
  }
}
