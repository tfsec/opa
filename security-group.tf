resource "aws_security_group" "sg_ssh_http_allow" { 
  vpc_id = "vpc-08c69146bbeb15644"
  name = "ssh-sg"
  # description = "Allow SSH and http inbound traffic"

  egress {
      from_port = 0
      to_port = 0
      protocol = -1
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
}



resource "aws_security_group" "lb" { 
  vpc_id = "vpc-08c69146bbeb15644"
  name = "ssh-sgsss"
  # description = "Allow SSH and http inbound traffic"

  egress {
      from_port = 0
      to_port = 0
      protocol = -1
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

}